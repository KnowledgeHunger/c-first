﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
      
            // setup
            var table = new List<Row> { new Row(id: 1, status: 10), new Row(id: 2, status: 20) };
            table.AddRange(generateRange(10000000));

            // randomize order
            Random rand = new Random();
            var models = table.OrderBy(c => rand.Next()).Select(c => c.id).ToList();


            // calculate
            var sw = Stopwatch.StartNew();

            var maxValue = table.Max(x => x.id);
            Console.WriteLine("Max: " + maxValue + " - Time: " + sw.ElapsedMilliseconds);

            var result = table.OrderByDescending(o => o.id).First(x => x.id == 9999999);
            Console.WriteLine("Orderby + First: " + result.id + " - Time: " + sw.ElapsedMilliseconds);

        }

        private static IEnumerable<Row> generateRange(int v)
        {
            List<Row> list = new List<Row>();
            for (int i = 0; i < v; i++)
            {
                list.Add(new Row(i, 1));
            }
            return list;
        }

        private class Row
        {
            public int id;
            public int status;

            public Row(int id, int status)
            {
                this.id = id;
                this.status = status;
            }
        }


    }
}
